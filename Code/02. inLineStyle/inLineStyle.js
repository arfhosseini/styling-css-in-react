import React from 'react';

const divStyle = {
  margin: '20px',
  border: '4px dashed blue'
};
const pStyle = {
  fontSize: '14px',
  textAlign: 'center'
};

const Box = () => (
  <div style={divStyle}>
    <p style={pStyle}>inLine style</p>
  </div>
);

export default Box;
