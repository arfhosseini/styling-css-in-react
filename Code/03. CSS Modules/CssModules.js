import React from 'react';
import styles from './Box.css';

const Box = () => (
  <div className={styles.main}>
    <p className={styles.content}>CSS Modules style</p>
  </div>
);

export default Box;
