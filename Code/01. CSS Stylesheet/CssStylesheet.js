import React from 'react';
import './Style.css';

const Box = () => (
  <div className="Box">
    <p className="BoxContent">CSS styling</p>
  </div>
);

export default Box;
