import React from 'react';
import styled from 'styled-components';

const Div = styled.div`
  margin: 20px;
  border: 4px dashed blue;
  &:hover {
   background-color: silver;
 }
`;

const Paragraph = styled.p`
  font-size: 14px;
  text-align: center;
`;

const Box = () => (
  <Div>
    <Paragraph>Styled-Components</Paragraph>
  </Div>
);

export default Box;
